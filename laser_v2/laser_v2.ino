//test
#include <SD.h>


#include <AccelStepper.h>

AccelStepper stepperX(1, 4 ,5); //(1, step , dir), EasyDriver 1/8 microstepping 
AccelStepper stepperY(1, 6, 7);//A4988driver full step

const int resetPin = A5;
unsigned long milli=0;
const int enableX = 2;
const int enableY = 3;
const int buttonY = 9;
const int buttonX = 8;
int fByte = 0;
int inByte = 0;
int i = 0;
int j = 0;
char stopByte; 
int count=1;
int post_count=1;


//OCTOBER 2017
int posnum; 
int extra = 0;
int post_enable[24];
  int final_post=1;
  int scheck_flag=1;

boolean Stop = true;

String filename = "data_";
int fileindex = 0;

//new functions for laser control
void laser();
void getinfo();
void dataon();
void dataoff();
void setextinput();
void trigger();
void scanrate();
void end_experiment();

//previous functions
void move_down();
void move_up();
void move_right();
void experiment();
void scan();
void initi();
void init_on_first();
void metrisi();
void stopit();
int chipSelect = 10;
int newproject = 0;
void setup(){
  //IN CASE BUTTON IS PRESSED THEN NEW PROJECT IS CREATED
 //if (digitalRead(buttonX) == HIGH || digitalRead(buttonY) == HIGH) newproject=1;
  //
  //AUTO RESET SETUP
digitalWrite(resetPin, HIGH);
delay(200);
pinMode(resetPin, OUTPUT); 
  //
  //Serial.setTimeout(100);
  Serial.begin(115200);
  stepperY.setMaxSpeed(10000.0);
  stepperY.setAcceleration(5000.0);
  stepperX.setMaxSpeed(10000.0);
  stepperX.setAcceleration(5000.0);
  pinMode(buttonX, INPUT);
  pinMode(buttonY, INPUT);
  pinMode(enableX, OUTPUT);
  pinMode(enableY, OUTPUT);
  
  digitalWrite(enableY,LOW);
  digitalWrite(enableX, LOW); // high dn douleuei
  //initialize();
Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");
//ADDING OLD TIME FOR NEW RESET
if (newproject) SD.remove("millis.txt"); else {
 File dataFile = SD.open("millis.txt");

  
  if (dataFile) {
    int mil[10];
    int i = 0;
    while (dataFile.available()) {
      int data = dataFile.read();
      if (data >= 48 && data <= 57) {
      mil[i] = data-48;
      i++;
      }
      else break;
    }
    int k=0;
    for (int j=i-1; j>=0; j--){
      milli=milli+mil[j]*pow(10,k);
   
    k++;
    }
    milli++;
    dataFile.close();
  }
}


//OCTOBER 2017 POST SELECTOR 
  File dFile = SD.open("posts.txt");

  int i=0;

  if (dFile) {
    while (dFile.available()) {
    String list = dFile.readStringUntil('\n');

if (list.startsWith("1")){post_enable[i] = 1; Serial.print("Activating post #"); Serial.println(i+1); if(i > final_post) final_post=i;}
    i++; 
    }
    dFile.close();
  }
  // if the file isn't open, pop up an error:
  else {
    for (int i=0; i<24; i++) post_enable[i] = 1;
    scheck_flag=0;
    Serial.println("posts.txt not detected, will scan all posts"); 
  }
  

//

/*
while (SD.exists(filename + fileindex + ".txt")) {
  fileindex++;
}

Serial.println(filename + fileindex + ".txt");
*/

/*
// PALIES EDOLES
delay(500);
laser();
setextinput();
delay(200);
laser();
dataon();
delay(500);
laser();
getinfo();*/ 

//KENURGIES EDOLES
delay(600);
laser();
setextinput();
delay(200);
laser();
scanrate();
delay(200);
laser();
settime();
delay(200);
laser();
getinfo();
 
 // ARXI PEIRAMATOS
 Serial.println("Starting experiment..");
   //inByte='s';
   initi();  //steilto sthn arxh na akoumphsei kai ta duo buttons
   init_on_first(); //steilto apo ekei sto kentro toy 1ou post
   delay(500);
   experiment(); 
 end_experiment();
}

void loop()
{

   
 /*

  if (Serial.available() > 0)
  {
    inByte = Serial.read();                  
  }
   if(inByte == 'i'){
   initi();
   }
   if(inByte == 'F'){
    Serial.println("Stoping experiment..");
   stopit();
   }
   //inByte='E';
   //stopByte='M';
  if(inByte == 'E'){
     
Serial.println("Starting experiment..");
   //inByte='s';
   initi();  //steilto sthn arxh na akoumphsei kai ta duo buttons
   init_on_first(); //steilto apo ekei sto kentro toy 1ou post
   delay(500);
   experiment(); 
   delay(500);
  }
*/
}

//subrutines 
void init_on_first() //initialize over 1st posts center. Oles oi routines 8ewroun arxh auto
{   
  digitalWrite(enableY, LOW);
  stepperY.move(-6500);//-7200
  stepperY.runToPosition();
  digitalWrite(enableY, HIGH);
  delay(500);
  digitalWrite(enableX, LOW);
  stepperX.move(11700);//10800
  stepperX.runToPosition();
  digitalWrite(enableX, HIGH);
  delay(500);
}

void move_down() //Move from one post's center to the other's. Down= 1 kinhsh
{
  digitalWrite(enableY, LOW);
  stepperY.move(-31000);
  stepperY.runToPosition();
  digitalWrite(enableY, HIGH);
  delay(500);  
  stopit();
}

void move_right() //Move from one post's center to the other's. right= 2 kinhsh
{   
  digitalWrite(enableX, LOW);
  stepperX.move(31050);
  stepperX.runToPosition();
  digitalWrite(enableX, HIGH);
  delay(500);  
  stopit();
}
void move_up() //Move from one post's center to the other's. up= 3 kinhsh
{
  digitalWrite(enableY, LOW);
  stepperY.move(31000);
  stepperY.runToPosition();
  digitalWrite(enableY, HIGH);
  delay(500);
  stopit();  
}

void experiment() //consider laser initialized over the first post's center
{
  extra=0;
	count=1;  //metraw 8ades me omoies kinhseis. Exw 3 tetoies 8ades sta 24 postakia
	post_count=1; //metraw to postaki panw apo to opoio eimai
	check1:
		   scan();
		   post_count=post_count+1;
		   move_down();
		   if (post_count<3)
			{
				goto check1;
			}
       scan(); //kanw scan sto #3
		   move_down(); //vriskomai sto #4
		   post_count=post_count+1;
		   scan();
		   extra++;
		   move_right(); //eimai sto #5
       scan();
	check2:
		   post_count=post_count+1;
		   move_up(); 
		   scan();
		   if (post_count<7)
  	   {
			    goto check2;
   	   }
		   post_count=post_count+1;
       count=count+1;  //proxwraw sthn epomenh 8ada
		   if (count<4 ) 
		   {
	       move_right(); //eimai sto #9
         post_count=1; //8etw to post_count=1 gia na 3anarxisw
         goto check1;
 		   }
		   stopit();
       delay(1000);
		   
}



void scan(){
 if(post_count +(count-1)*8 - count + extra > final_post  && scheck_flag){
         
         end_experiment();
 }
 
 
 
 if (post_enable[post_count +(count-1)*8 - count + extra] == 1) {
  digitalWrite(enableY, LOW);
  stepperY.move(3500);//3700
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH); 
  
  delay(1100); 
  stopit();
  //alagh
posnum=1; //OCTOBER 2017
      metrisi();//  1  
 
  
  digitalWrite(enableY,LOW);
  stepperY.move(-1700);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(500);
  digitalWrite(enableX,LOW);
  stepperX.move(3200);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(1100);
  stopit();
  //alagh
 posnum=2; //OCTOBER 2017
      metrisi();//  2
  
  

  digitalWrite(enableY,LOW);
  stepperY.move(-1800);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(500);
  digitalWrite(enableX,LOW);
  stepperX.move(300);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(1100);
  
  stopit();
  //alagh
 posnum=3; //OCTOBER 2017
      metrisi();// 3  
   

  digitalWrite(enableX,LOW);
  stepperX.move(-300);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(500);
  digitalWrite(enableY,LOW);
  stepperY.move(-1800);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(1100);
  
  stopit();
  //alagh
posnum=4; //OCTOBER 2017
      metrisi();//  4  
   
  digitalWrite(enableX,LOW);
  stepperX.move(-3200);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(500);
  digitalWrite(enableY,LOW);
  stepperY.move(-1700);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(1100);
  
  stopit();
  //alagh
  posnum=5; //OCTOBER 2017
      metrisi();//  5
   
  digitalWrite(enableY,LOW);
  stepperY.move(1700);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(500);
  digitalWrite(enableX,LOW);
  stepperX.move(-3200);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(1100);
  
  stopit();
  //alagh
posnum=6; //OCTOBER 2017
      metrisi();//  6 
    
  digitalWrite(enableY,LOW);
  stepperY.move(1800);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(500);
  digitalWrite(enableX,LOW);
  stepperX.move(-300);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(1100);
  
  stopit();
  //alagh
  posnum=7; //OCTOBER 2017
      metrisi();//  7
    
digitalWrite(enableX,LOW);
 stepperX.move(300);
stepperX.runToPosition();
digitalWrite(enableX,HIGH);
delay(500);
  digitalWrite(enableY,LOW);
  stepperY.move(1800);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(1100);
  
  stopit();
  //alagh
posnum=8; //OCTOBER 2017
      metrisi();//  8
 
  digitalWrite(enableX,LOW);
  stepperX.move(3200);
  stepperX.runToPosition();
  digitalWrite(enableX,HIGH);
  delay(500);
  digitalWrite(enableY,LOW);
  stepperY.move(-1800);
  stepperY.runToPosition();
  digitalWrite(enableY,HIGH);
  delay(1100);
  //   back to the center
}
}
void metrisi(){
  
  
  long input;
  

//Buffer recycling
for (int i=0; i<64; i++) Serial.read();

     
  
 //if (Serial.available() > 0) {
    while(1){
         if (Serial.read() == 13) break;
    }
    input = Serial.parseInt();

    while (input > 17000 || input<1000) input = Serial.parseInt(); //correction 4 first problematic values and wrong readouts due to buffer overfilling
 
     for (int i=0; i<50; i++){
   if (input >1000 && input < 10000) break;
    
       input = Serial.parseInt();
     }
    //if (Serial.read() == 13) input = Serial.parseInt();
    
     //if (Serial.read() == 13) {
      //Serial.print("raw data: ");
    //  Serial.println(input); 

      String timestamp = "";
unsigned long mil = milli+millis();
unsigned long hours = mil/3600000;
mil = mil - hours*3600000;
unsigned long minutes = mil/60000;
mil = mil - minutes*60000;
unsigned long seconds = mil/1000;
mil = mil - seconds*1000;
timestamp += "  ";
timestamp += hours;
timestamp += ":";
timestamp += minutes;
timestamp +=":";
timestamp +=seconds;

//OCTOBER 2017 ADDITIONS
timestamp += "   #Post: ";
timestamp += post_count +(count-1)*8 - count  +1 + extra;
timestamp += " #Position: ";
timestamp += posnum;


String output ="";
//  double value = input;
   if (input < 16367)   {

//value = (((value * (1.02000 / 16368)) - 0.01000) * 10);

output += input;
output += timestamp;


}
else{
output +="Error meas";
output += timestamp;

  
}
Serial.println(output);
File file;
file = SD.open(filename + fileindex + ".txt", FILE_WRITE);
if (file){
file.println(output);
file.close();
//}
}
        //  break;    
    //}
  //}



}
void stopit(){
//while (digitalRead(buttonX) == LOW || digitalRead(buttonY) == LOW){
  digitalWrite(enableX, LOW);
  stepperX.setSpeed(0);
  stepperX.runSpeed();
  digitalWrite(enableX, HIGH);
  digitalWrite(enableY,LOW);
  stepperY.setSpeed(0);
  stepperY.runSpeed();
  digitalWrite(enableY, HIGH);
}
void laser(){
Serial.write(43);
Serial.write(43);
Serial.write(43);
Serial.write(13);
Serial.write(73);
Serial.write(76);
Serial.write(68);
Serial.write(49);
} 
void getinfo(){
Serial.write(32);
Serial.write(73);
Serial.write(0);
Serial.write(2);
}

void dataon(){
Serial.write(32);
Serial.write(119);
Serial.write(0);
Serial.write(2);
}

void dataoff(){
Serial.write(32);
Serial.write(118);
Serial.write(0);
Serial.write(2);  
}

void settime(){
Serial.write(32);
Serial.write(245);
Serial.write(0);
Serial.write(3);
Serial.write(0);
Serial.write(0);
Serial.write(0);
Serial.write(255);

}
void setextinput(){
Serial.write(32);
Serial.write(244);
Serial.write(0);
Serial.write(3);
Serial.write(0);
Serial.write(0);
Serial.write(0);
Serial.write(1);
}

void scanrate(){
Serial.write(32);
Serial.write(133);
Serial.write(0);
Serial.write(3);
Serial.write(0);
Serial.write(0);
Serial.write(0);
Serial.write(4);
}

/*
void setextinput(){
Serial.write(32);
Serial.write(244);
Serial.write(0);
Serial.write(3);
Serial.write(0);
Serial.write(0);
Serial.write(0);
Serial.write(2);
}

void laser(){
Serial.write(43);
Serial.write(43);
Serial.write(43);
Serial.write(13);
Serial.write(73);
Serial.write(76);
Serial.write(68);
Serial.write(49);
} 
void getinfo(){
Serial.write(32);
Serial.write(73);
Serial.write(0);
Serial.write(2);
}

void dataon(){
Serial.write(32);
Serial.write(119);
Serial.write(0);
Serial.write(2);
}

void dataoff(){
Serial.write(32);
Serial.write(118);
Serial.write(0);
Serial.write(2);  
}
*/
void end_experiment(){
   delay(500);
   Serial.println("End of experiment :)");
   initi();
   delay(500);
   //TELOS

//FINAL RESET
SD.remove("millis.txt");


File myFile = SD.open("millis.txt", FILE_WRITE);

 if (myFile) {
    Serial.print("Writing to millis.txt...");
    myFile.println(milli+millis());
    // close the file:
    myFile.close();
  
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening millis.txt");
  }





  //RESET TO AVOID PROBLEMS_START
 digitalWrite(resetPin, LOW);
}


void initi(){
while (digitalRead(buttonX) == LOW || digitalRead(buttonY) == LOW){
if(digitalRead(buttonY) == LOW)   
{
  digitalWrite(enableY, LOW);
  stepperY.setSpeed(5000);
  stepperY.runSpeed();
}
else{
  digitalWrite(enableY, HIGH);
}

if (digitalRead(buttonX) == LOW)
{
  digitalWrite(enableX, LOW);
  stepperX.setSpeed(-5000);
  stepperX.runSpeed();
}
 else{
  digitalWrite(enableX, HIGH);
 }
}
}
